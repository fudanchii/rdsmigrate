package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/garyburd/redigo/redis"
)

type config struct {
	Sources []string `json:"sources"`
	Target  string   `json:"target"`
	Keys    []string `json:"keys"`
}

type migrator struct {
	config
	TargetPool *redis.Pool
}

var counter uint64 = 0

func readConfig(cf string) (icfg config) {
	ct, err := ioutil.ReadFile(cf)
	if err != nil {
		log.Fatal(err.Error())
	}
	err = json.Unmarshal(ct, &icfg)
	if err != nil {
		log.Fatal(err.Error())
	}
	return
}

func poolConnect(server string, params ...string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			if params != nil && len(params[0]) > 0 {
				password := params[0]
				if _, err := c.Do("AUTH", password); err != nil {
					c.Close()
					return nil, err
				}
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func (m *migrator) getKeys(conn redis.Conn, pattern string) ([]string, error) {
	rkc, err := conn.Do("KEYS", pattern)
	return redis.Strings(rkc, err)
}

func (m *migrator) checkAndMigrate(sconn redis.Conn, keys []string) (err error) {
	tconn := m.TargetPool.Get()
	defer tconn.Close()
	for _, k := range keys {
		rc, err := tconn.Do("EXISTS", k)
		if v, _ := redis.Int(rc, err); v == 1 {
			fmt.Printf("%s exists.\n", k)
			continue
		}
		rc, err = sconn.Do("DUMP", k)
		cnt, err := redis.Bytes(rc, err)
		if err != nil {
			log.Println(err.Error())
			continue
		}
		if _, err = tconn.Do("RESTORE", k, 0, cnt); err != nil {
			log.Println(err.Error())
			continue
		}
		fmt.Printf("%s migrated.\n", k)
		counter += 1
	}
	return
}

func (m *migrator) migrate(conn redis.Conn, err error) error {
	if err != nil {
		return err
	}
	for _, k := range m.Keys {
		keys, err := m.getKeys(conn, k)
		if err != nil {
			continue
		}
		err = m.checkAndMigrate(conn, keys)
	}
	return err
}

func (m *migrator) Migrate() {
	m.TargetPool = poolConnect(m.Target)
	for _, srv := range m.Sources {
		conn, err := redis.Dial("tcp", srv)
		err = m.migrate(conn, err)
		if err != nil {
			log.Println(srv + ": " + err.Error())
		}
		conn.Close()
	}
}

func main() {
	cfg := readConfig("rdsmigrate.json")
	mgr := &migrator{config: cfg}
	mgr.Migrate()
	fmt.Println()
	fmt.Printf("%d keys migrated.\n", counter)
}
