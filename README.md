rdsmigrate
---

A tool for migrating redis data to another instance, e.g. migrating sharded rdb to a single instance, or just in case when you don't have access to the rdb file.

(public domain)
